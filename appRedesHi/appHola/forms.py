#Aqui va a ir la logica de los forms para la DB

from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms


class CreateUserLogAndForm(UserCreationForm):
    """
    Clase para manejar datos del form de registro y regitro por que
    estan en la misma pagina
    """
    def __init__(self, *args, **kwargs):
        super(CreateUserLogAndForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget = forms.PasswordInput(attrs={'placeholder': 'Contraseña'})
        self.fields['password2'].widget = forms.PasswordInput(attrs={'placeholder': 'Confirmar contraseña'})

    class Meta:
        model = User
        fields = ['username','email','password1', 'password2']
        widgets = {
            'username': forms.TextInput(attrs={'placeholder': 'Nombre'}),
            'email': forms.TextInput(attrs={'placeholder': 'Email'})
        }