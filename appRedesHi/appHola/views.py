# Create your views here.
#Las vistas de la pagina web,i.e los html van aqui
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.views import View
from django.http import HttpResponseRedirect
from django.contrib import messages

from django.contrib.auth.decorators import login_required

from .forms import CreateUserLogAndForm
from .funtions import saludo

def index(request):

    form = CreateUserLogAndForm()
    if request.method == 'POST':
        if request.POST.get('submit') == 'Registrarse':
            #aqui la logica del registro
            print("Le pushueo al Registrarse pero...")
            form = CreateUserLogAndForm(request.POST)
            if form.is_valid(): #django se encarga de la validacion
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Cuenta creada para ' + user)
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        elif request.POST.get('submit') == 'Acceder': #oh shit no esta desaoplado, referente al value
            #aqui la logica del login
            print("Le pushueo al Login pero...")
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request,user)
                return redirect(home) #a no mames
            else:
                messages.info(request, 'Usuario o contraseña no validos')
    
    context = {'form':form}
    return render(request, 'index.html',context)


@login_required(login_url=index)
def home(request):
    context = {}
    context['msg'] = saludo()
    print(saludo())
    return render(request, 'hola.html', context)
    

def LogOut(request):
    logout(request)
    return redirect('appHola:index')

